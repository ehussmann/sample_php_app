
## About

A sample project that demonstrates usage of Laravel to create a microservice API, interact with a third-party (Zendesk) API, and a client side javascript app that consumes the Laravel API.