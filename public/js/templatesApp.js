var xcsrf = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: {'X-CSRF-Token': xcsrf}
});


$(document).ajaxStart(function(){
	$('.progress').css({'visibility': 'visible'});
});

$(document).ajaxStop(function(){
	$('.progress').css({'visibility': 'hidden'});
});

$('table').stickyTableHeaders();

var TemplateCollection = Backbone.Collection.extend();

var TemplateFieldModel = Backbone.Model.extend({
	urlRoot: '/value'
});


var TemplateFieldView = Backbone.View.extend({
	tagName: 'td',
	template: _.template($('#template-text-field').html()),
	selectTemplate: _.template($('#template-select-field').html()),
	render: function() {
		var field = _.findWhere(fields, {id: this.model.attributes.template_field_id});
		if(field.is_tagger == 'yes'){
			var opts = JSON.parse(field.tagger_options);
			for (var i = 0; i < opts.length; i++) {
				opts[i].selected = (this.model.attributes.value == opts[i].value) ? 'selected' : '';
			};
			this.model.set({options: opts});
			this.$el.html(this.selectTemplate(this.model.attributes));
		} else {
			this.$el.html(this.template(this.model.attributes));
		}
    	return this;
	},

	events: {
		"keyup input" : function(e) {
			this.toggleSave();
			this.setModelAttribute(e);
		},
		"change select" : function(e) {
			this.toggleSave();
			this.setModelAttribute(e);
		},
	},

	setModelAttribute: function(e){
		var el = this.el;
		var val = $(el).find('.val-prop').val();

		this.model.set({value: val, updated: true});
		console.log(this.model);
	},

	toggleSave: function() {
		this.$el.closest('tr').find('.template-save').removeClass('disabled').find('i').text('input');
	},
});

var TemplateRowView = Backbone.View.extend({
	tagName: 'tr',
	template: _.template($('#template-row').html()),

	render: function() {
		// console.log(this);
		var row = this;
		this.$el.html(this.template(this.attributes));
		$('#template-rows').append(this.el);

		var $tnInput = this.$el.find('input.id-prop').parent('td');

		_.each(this.collection.models.reverse(), function(field){
			var fieldView = new TemplateFieldView({model: field});
			// console.log($tnInput);
			$tnInput.after(fieldView.render().el);
		})

		return this;
	},

	events: {
		"keyup input.id-prop" : function(e) {
			this.toggleSave();
			this.setTemplateNumber(e);
		},
		"click .template-save" : "saveTemplate",
		"click .template-delete": "deleteTemplate"
	},


	toggleSave: function() {
		this.$el.closest('tr').find('.template-save').removeClass('disabled').find('i').text('input')
	},

	setTemplateNumber: function(e) {
		var val = $(e.target).val();
		this.attributes.templateNumber = val;
		this.attributes.updated = true;
	},

	deleteTemplate: function() {
		var view = this;
		$.confirm({
			title: 'Are You Sure?',
			content: '',
			boxWidth: '30%',
    		useBootstrap: false,
			buttons: {
				confirm: {
					btnClass: 'green white-text',
					action: function(){
						$.ajax({
							url: 'template/'+view.attributes.id,
							type: 'delete',
							success: function(){
								view.remove();
							}
						});
					}
				},
				cancel: function(){
					return;
				}
			}
		});
			
	},

	saveTemplate: function() {		
			
		var view = this;
		var updatesCount = 0;
		var doneCount = 0;
		_.each(this.collection.models, function(field){
			if(field.attributes.updated) {
				updatesCount++;
				field.save({}, {
					success: function(){
						doneCount++;
						field.attributes.updated = false;
						console.log('Field Value Saved'); //TODO Wire in Zendesk Notifications

						if(doneCount == updatesCount) {
							console.log('done');
							view.$el.closest('tr').find('.template-save').find('i').text('check_circle')

						}
					},
					error: function(){
						console.log('Error Saving');//TODO Wire in Zendesk Notifications
						$('.error').removeClass('hide');
						setTimeout(function(){
							$('.error').fadeOut().addClass('hide');
						}, 3000);
					}
				});
			}
		});

		if(view.attributes.updated) {
			$.ajax({
				url: '/template/'+view.attributes.id,
				type: 'put',
				data: {identifier: view.attributes.templateNumber},
				success: function(){
					view.attributes.updated = false;
					console.log('Template Number Saved'); //TODO Wire in Zendesk Notifications
					view.$el.closest('tr').find('.template-save').find('i').text('check_circle');
				}
			});
		}
	}
});


var NewTemplateView = Backbone.View.extend({
	template: _.template($('#new-template-row').html()),
	tagName: 'tr',
	render: function(){
		this.$el.html(this.template());
		$('#template-rows').append(this.el);
	},

	events: {
		"keyup input.id-prop" : "toggleSave",
		"click .new-template-save" : "createTemplate",
		"click .template-delete": "deleteTemplate",
	},

	toggleSave: function() {
		this.$el.closest('tr').find('.new-template-save').removeClass('disabled');
	},

	createTemplate: function() {
		var inputs = {};
		var newView = this;

		this.$el.find('.prop').each(function(){
			var fieldId = $(this).data('field-id');
			var val = $(this).val();
			inputs[fieldId] = val;
			//inputs.push({fieldId:fieldId, value: val});
		});

		$.ajax({
			url: '/template',
			type: 'post',
			data: inputs,
			success: function(data){
				var template = JSON.parse(data);
				newView.remove();
				displayTemplates([template]);
			}
		})
	},

	deleteTemplate: function() {
		var view = this;
		$.confirm({
			boxWidth: '30%',
    		useBootstrap: false,
			title: 'Are You Sure?',
			content: '',
			buttons: {
				confirm: {
					btnClass: 'green white-text',
					action: function(){ 
						view.remove();
					},
				},
				cancel: function(){
					return;
				}
			}
		});
	}

});

// var allTemplates = new TemplatesCollection(templates);

function displayTemplates(templates) {
	_.each(templates, function(template){
		var templateFields = [];

		_.each(template.values, function(property){
			var propModel = new TemplateFieldModel(property);
			templateFields.push(propModel);
		});

		var templateCol = new TemplateCollection(templateFields);
		templateCol.templateNumber = template.identifier;

		var templateRowView = new TemplateRowView({collection: templateCol, attributes: {templateNumber: templateCol.templateNumber, id: template.id}});
		templateRowView.render(); 
		
	});	
}

displayTemplates(templates);


$('#add-row').click(function(){
	var newTempView = new NewTemplateView()
	newTempView.render();
});