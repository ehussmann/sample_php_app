var xcsrf = $('meta[name="csrf-token"]').attr('content');

// var client = ZAFClient.init();

$.ajaxSetup({
    headers: {'X-CSRF-Token': xcsrf}
});


$(document).ajaxStart(function(){
	$('.progress').css({'visibility': 'visible'});
});

$(document).ajaxStop(function(){
	$('.progress').css({'visibility': 'hidden'});
});

 

var FieldModel = Backbone.Model.extend({
	urlRoot: 'field',
	validate: function(attrs) {
		if(!attrs.name || !attrs.zendesk_field_id) {
			return "Must Set Field Name and Field Mapping";
		}
	}
});

var FieldsCollection = Backbone.Collection.extend({
	model: FieldModel
});

var FieldView = Backbone.View.extend({
	attributes: {'class': 'row'}, 
	template: _.template($('#field-template').html()),
	events: {
		"change select" : "updateZDMap",
		"keyup .input-name": "updateFieldName",
		"mouseenter": "showDelete",
		"mouseleave": "hideDelete",
		"click .field-delete": "deleteModel"
	},
	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		this.renderSelect();
		return this;
	},
	renderSelect: function(){
		var model = this.model;
		var $select = this.$el.find('select');
		$select.find('option').each(function(){
			if($(this).val() == model.attributes.zendesk_field_id) {
				$(this).attr('selected', 'selected');
			}
		});
	},
	updateZDMap: function(){
		var $select = this.$el.find('select');
		var fieldId = $select.val();
		this.model.set({zendesk_field_id: fieldId});
		var zdField = _.findWhere(zdFields, {id: parseInt(fieldId)});

		if(zdField.type == 'tagger') {
			this.model.set({is_tagger: 'yes', tagger_options: zdField.custom_field_options});
		} else if(zdField.type == 'date') {
			this.model.set({is_date: 'yes', is_tagger: 'no'});
		} else {
			this.model.set({is_tagger: 'no'});
		}

		if(!this.model.attributes.newModel){
			this.model.set({updated:true});
		}
		$('.save-button').find('i').text('save');
	},
	updateFieldName: function() {
		var val = this.$el.find('.input-name').val();
		this.model.set({name: val});
		if(!this.model.attributes.newModel){
			this.model.set({updated:true});
		}
		$('.save-button').find('i').text('save');
	},
	showDelete: function(){
		this.$el.find('.field-delete').removeClass('hide');
	},
	hideDelete: function(){
		this.$el.find('.field-delete').addClass('hide');
	},
	deleteModel: function() {
		var view = this;
		$.confirm({
			title: 'Are You Sure?',
			content: '',
			boxWidth: '30%',
    		useBootstrap: false,
			buttons: {
				confirm: {
					btnClass: 'green white-text',
					action: function(){
						view.model.destroy({
							success: function(){
								view.remove();
							}
						});
					}
				},
				cancel: function(){
					return;
				}
			}
		});
	}

});

var FormView = Backbone.View.extend({
	el: $('#fields-app'),
	events: {
		"click .save-button": "saveModels",
		"click #add-field" : "addNew",
	},
	render: function(){
		var view = this;
		//this.collection.reverse();
		_.each(this.collection.models, function(field){
			view.addField(field);
		});
	},

	addNew: function(){
		var field = new FieldModel;
		field.set({newModel: true, name:'', zendesk_field_id: ''});
		this.collection.add(field);
		this.addField(field);
	},

	addField: function(field){
		var view = new FieldView({model: field});
		this.$el.find('.fields').append(view.render().el);
	},

	saveModels: function(){
		var view = this;
		var updatesCount = 0;
		var doneCount = 0;
		_.each(this.collection.models, function(model){
			if(model.attributes.updated || model.attributes.newModel) {
				if(model.isValid()) {
					updatesCount++;
					model.save({}, {
						success: function(){
							doneCount++;
							if(doneCount == updatesCount) {
								console.log('done');
								$('.save-button').find('i').text('check_circle');
							}
						},
						error: function(){
							console.log('Error Saving');//TODO Wire in Zendesk Notifications
							$('.error').removeClass('hide');
							setTimeout(function(){
								$('.error').addClass('hide');
							}, 3000);
						}
					});
				} else {
					alert(model.validationError);
				}
			}
		});
	}
}); 

var Fields = new FieldsCollection(fields);

var form = new FormView({collection:Fields});
form.render();