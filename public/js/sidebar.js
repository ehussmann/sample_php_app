$(document).ready(function(){
	var client = ZAFClient.init();
	client.invoke('resize', { width: '100%', height: '300px' });

	client.get('ticket').then(function(resp){
    	var ticket = resp.ticket;

    	if(ticket.form.id != 375427) {
    		$('.content').html('<p>No app actions available</p>')
    	} else {

			$('#template-select').selectize();

			$('.template-apply').click(function(){
				var tempId = parseInt($('#template-select').val());
				var template = _.findWhere(templates, {id: tempId});
				var count = 0;

				client.set('ticket.customField:custom_field_58771808', template.identifier).then(function(){
					_.each(template.values, function(value){
						var field = _.findWhere(fields, {id: value.template_field_id});
						// console.log(field.zendesk_field_id+': '+value.value);
						client.set('ticket.customField:custom_field_'+field.zendesk_field_id, value.value).then(function(){
							count++;
							if(count == template.values.length) {
								client.invoke('notify', 'Template Data Applied');
							}
						});
					});
				});
			});
		}
	});
});