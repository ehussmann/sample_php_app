<?php

namespace App\Http\Middleware;

use Closure;

class CheckZDArgs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $app_guid = $request->input('app_guid');
        $origin = $request->input('origin');

        if(env('APP_ENV') == 'production'){
            if(is_null($app_guid) || !strlen($app_guid) || is_null($origin) || !strlen($origin)) {
                return response('Forbidden', 403);
            }
        }
        
        return $next($request);
    }
}
