<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;

class TemplateController extends Controller
{
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        array_pull($inputs, 'template-identifier');
        $template = new Template;
        $template->identifier = $request->input('template-identifier');
        $template->save();

        foreach($inputs as $fieldId => $val) {
            $template->values()->create([
                "template_id" => $template->id,
                "template_field_id" => $fieldId,
                "value" => $val 
            ]);
        }

        $template->load('values');
        return response($template->toJSON(), 200);        
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $value = $request->input('identifier');
            $template = Template::find($id);
            $template->identifier = $value;        
            $template->save();
            return response($template->toJSON(), 200);
        } catch(Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Template::destroy($id);
    }
}
