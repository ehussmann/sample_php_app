<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\TemplateField;
use App\Template;
use App\TemplateValue;
use Zendesk\API\HttpClient as ZendeskAPI;

class TemplateFieldController extends Controller
{

    public function __construct() {
        $this->zd = new ZendeskAPI(env('ZD_SUBDOMAIN'));
        $this->zd->setAuth('basic', ['username' => env('ZD_USERNAME'), 'token' => env('ZD_TOKEN')]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryVars['appGUID'] = $request->input('app_guid');
        $queryVars['origin'] = $request->input('origin');
        $fields = TemplateField::all();


        //look up the form to get attached field ids, then look up all the fields, and filter them down
        //to only the ones in the form
        $zdForm = $this->zd->ticketForms()->find(375427);
        $zdFormFields = collect($zdForm->ticket_form->ticket_field_ids);
        $zdFieldsReq = $this->zd->ticketFields()->findAll();
        $zdFields = collect($zdFieldsReq->ticket_fields);
        $formFields = $zdFields->filter(function($field) use ($zdFormFields){
            return $zdFormFields->contains($field->id);
        });
        
        return View::make('fields')->with(compact('fields'))->with(compact('queryVars'))->with(compact('formFields'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $field = new TemplateField;
            $field->name = $request->input('name');
            $field->zendesk_field_id = $request->input('zendesk_field_id');
            $field->is_tagger = $request->input('is_tagger');
            $field->tagger_options = json_encode($request->input('tagger_options'));
            $field->save();
                
            $templates = Template::all();
            foreach($templates as $tmpl) {
                $value = new TemplateValue;
                $value->template_id = $tmpl->id;
                $value->template_field_id = $field->id;
                $value->value = '';
                $value->save();
            }   

            return response($field->toJSON(), 200);

        } catch(Exception $e) {
            return response($e->getMessage(), 500);
        }


    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $options = $request->input('tagger_options');
       
        try {
            $name = $request->input('name');
            $zdField = $request->input('zendesk_field_id');
            
            $field = TemplateField::find($id);
            $field->name = $name;
            $field->zendesk_field_id = $zdField;
            $field->is_tagger = $request->input('is_tagger');
            $field->tagger_options = (is_array($options)) ? json_encode($options) : $options;        
            $field->save();
            
            return response($field->toJSON(), 200);
        } catch(Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return TemplateField::destroy($id);
    }
}
