<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Template;
use App\TemplateField;
use App\TemplateValue;

class Sidebar extends Controller
{
    public function __invoke(Request $request) {
    	$queryVars['appGUID'] = $request->input('app_guid');
    	$queryVars['origin'] = $request->input('origin');

    	$templates = Template::with('values')->get();
    	$fields = TemplateField::all();

 		return View::make('sidebar')->with(compact('templates'))->with(compact('fields'))->with(compact('queryVars'));
    }
}
