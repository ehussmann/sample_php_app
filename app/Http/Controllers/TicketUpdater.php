<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection as Collection;
use Zendesk\API\HttpClient as ZendeskAPI;
use App\Template;
use App\TemplateField;
use App\TemplateValue;


class TicketUpdater extends Controller
{

	private $nextPage = null;
	private $allResults;
	private $searchQuery;

	public function __construct() {
		$this->zd = new ZendeskAPI(env('ZD_SUBDOMAIN'));
        $this->zd->setAuth('basic', ['username' => env('ZD_USERNAME'), 'token' => env('ZD_TOKEN')]);
	}

    public function populateTemplate(Request $request) {

    	$ticket_id = $request->input('ticket_id');
    	$template_identifier = $request->input('template_number');
    	return $this->_applyTemplate($template_identifier, $ticket_id);

    }

    private function _applyTemplate($template_identifier, $ticket_id, $updates = [], $tags = []) {
    	$template = Template::where('identifier', $template_identifier)->with('values')->first();

    	if(!is_null($template)) {
	    	$fields = TemplateField::all();
	    	$values = $template->values()->get();
	    	// $updates = [];
	    	foreach($fields as $field) {
	    		$value = $values->where('template_field_id', $field->id)->first();

	    		// dump($value->toArray());

	    		if($field->is_date == 'yes') {
	    			$value->value = date('Y-m-d', strtotime($value->value));
	    		}
	    		// $updates[$field->zendesk_field_id] = $value->value;
	    		$setting = ['id' => (int)$field->zendesk_field_id, 'value' => $value->value];
	    		array_push($updates, $setting);
	    	}

	    	try {
	    		if(empty($tags)) {
	    			$update = $this->zd->tickets()->update($ticket_id, ['custom_fields' => $updates]);
	    		} else {	
	    			$update = $this->zd->tickets()->updateMany([
	    				'ids'=>[$ticket_id], 
	    				'custom_fields' => $updates, 
	    				'additional_tags' => $tags
	    			]);
	    		}
	    		Log::info('Template '.$template_identifier.' applied to '.$ticket_id);
	    		return response(json_encode($update), 200);
	    	} catch (\Zendesk\API\Exceptions\ApiResponseException $e) {
	    		Log::error($e->getMessage());
	    		return response($e->getMessage(), 500);
	    	}
	    	
	    } else {
	    	$update = $this->zd->tickets()->updateMany(
	    		[
		    		'ids' => [$ticket_id],
					'custom_fields' => ['id' => 51752687,'value' => 'crpr_review_needed'],
					'additional_tags' => ['incorrect_template_file_number']
				]
	    	);
	    	Log::info('CRPR with incorrect template', ['ticket' => $ticket_id, 'data' => [$update]]);
	    }
    }

    public function validateTemplate(Request $request)
    {
    	$template_identifier = $request->input('template_id');

    	$template = Template::where('identifier', $template_identifier)->first();

    	if(is_null($template)) {
    		return response()->json(['found' => false], 200)->withCallback($request->input('callback'));;
    	}

    	return response()->json(['found' => true], 200)->withCallback($request->input('callback'));;
    }


}
