<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\TemplateValue;

class TemplateValueController extends Controller
{
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $value = $request->input('value');
            $field = TemplateValue::find($id);
            $field->value = $value;        
            $field->save();
            return response($field->toJSON(), 200);
        } catch(Exception $e) {
            return response($e->getMessage(), 500);
        }

    }


}
