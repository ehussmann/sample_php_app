<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function values()
    {
        return $this->hasMany('App\TemplateValue');
    }
}
