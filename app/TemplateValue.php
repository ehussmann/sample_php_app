<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateValue extends Model
{
    protected $fillable = ['tempalte_id', 'template_field_id', 'value'];
}
