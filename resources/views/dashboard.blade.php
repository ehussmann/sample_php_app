<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">            
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.css">
        <style>
            .content {
                padding: 20px;
            }
            thead {
                background-color: #efefef;
            }
            table, input.prop {
                font-size: 10px !important;
            }
            select {
                display: block;
            }
            table.highlight>tbody>tr:hover {
                background-color: #e9f0f9;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="progress" style="visibility: hidden;">
              <div class="indeterminate"></div>
            </div>
            <div class="card-panel error hide red accent-2" style="padding: 10px">
                <p class="white-text">Oops, something went wrong!</p>
            </div>
            <div class="row">
                <a href="{{ url('field?app_guid='.$queryVars['appGUID'].'&origin='.$queryVars['origin']) }}" class="waves-effect waves-light btn-flat green white-text right">Edit Fields</a>
            </div>
            <table class="highlight bordered striped">
                <thead>
                    <th>Template Number</th>
                    @foreach($fields as $col)
                        <th data-zd-id="{{$col->zendesk_field_id}}">{{$col->name}}</th>
                    @endforeach
                    <th></th>
                    <th></th>
                </thead>
                <tbody id="template-rows">
                </tbody>
                  
            </table>
            <a style="margin-top: 25px; margin-right: 25px" id="add-row" class="btn-floating waves-effect waves-light green right"><i class="material-icons">add</i></a>
            
        </div>
        

        <script type="text/template" id="template-text-field">
            <input class="prop val-prop" type="text" data-field-id="<%= id %>" value="<%= value %>" />
        </script>

        <script type="text/template" id="template-select-field">
            <select class="prop val-prop" type="text" data-field-id="<%= id %>">
            <option value="">--</option>
            <% _.each(options, function(opt) { %>
                <option value="<%= opt.value %>" <%= opt.selected %>><%= opt.name %>
            <% }); %>
            </select>
        </script>

        <script type="text/template" id="template-row">
            <td>
                <input class="prop id-prop" type="text" value="<%= templateNumber %>" /></td>            
            <td>
                <a class="waves-effect waves-light btn-floating ex-small blue disabled template-save"><i class="material-icons">input</i></a>
            </td>
            <td>
                <a class="waves-effect waves-light btn-floating ex-small red template-delete "><i class="material-icons">delete</i></a>
            </td>

            
        </script>

        <script type="text/template" id="new-template-row">
            <td><input class="prop id-prop" data-field-id="template-identifier" type="text" value="" /></td>            
            @foreach($fields as $f)
            <td>
                @if($f->is_tagger == 'yes')
                    <select class="prop val-prop" data-field-id="{{$f->id}}">
                    <option value="">--</option> 
                    <?php 
                        $opts = json_decode($f->tagger_options);
                    ?>
                    @foreach($opts as $opt)
                        <option value="{{$opt->value}}">{{$opt->name}}</option>
                    @endforeach
                @else
                    <input class="prop val-prop" type="text" data-field-id="{{ $f->id }}" value="" />
                @endif
            </td>
            @endforeach           
            
            <td>
                <a class="waves-effect waves-light btn-floating ex-small blue disabled new-template-save"><i class="material-icons">input</i></a>
            </td>
            <td>
                <a class="waves-effect waves-light btn-floating ex-small red  template-delete"><i class="material-icons">delete</i></a>
            </td>
            
        </script>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/underscore.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/backbone.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/jquery.stickytableheaders.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.js"></script>
        <script>
            var templates = {!! $templates->toJson() !!};
            var fields = {!! $fields->toJson() !!};
        </script>
        <script type="text/javascript" src="{{ asset('js/templatesApp.js') }}"></script>
    </body>
</html>
