<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">            
        <link rel="stylesheet" href="{{ asset('css/selectize.css') }}">
        <style>
            .content {
                padding: 20px;
            }
            
            select {
                display: block;
            }
            .input-field label {
                position: relative !important;
                top: 0 !important;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="input-field col s6">
                <label for="template-select">Select a Template to Apply to the Ticket</label>
                <select name="template-select" id="template-select">
                    <option value="">-Select a Template</option>
                    @foreach($templates as $template)
                        <option value="{{$template->id}}">{{$template->identifier}}</option>
                    @endforeach
                </select>
            </div>
            <a class="waves-effect waves-light btn blue template-apply">Apply</a>
        </div>
        <script>
            var templates = {!! $templates->toJson() !!};
            var fields = {!! $fields->toJson() !!};
        </script>
        <script src="https://assets.zendesk.com/apps/sdk/2.0/zaf_sdk.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/underscore.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/selectize.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/sidebar.js') }}"></script>
    </body>
</html>