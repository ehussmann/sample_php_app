<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">            
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.css">
        <style>
            .content {
                padding: 20px;
            }
            thead {
                background-color: #fefefe;
            }

            #fields-app {
                width: 800px;
                margin: 0 auto;
            }
            .field-delete {
                margin-top: 25px;
            }
        </style>
    </head>
    <body>
        <div class="content">

            <div class="row">
                <a href="{{ url('dashboard?app_guid='.$queryVars['appGUID'].'&origin='.$queryVars['origin']) }}" class="waves-effect waves-light btn gray left"></i>Back</a>
            </div>

            <div class="row">
                <div id="fields-app" >
                    <div class="fields"></div>
                    <a class="waves-effect waves-light btn green right save-button"><i class="material-icons left">save</i>Save</a>
                    <a id="add-field" class="btn-floating waves-effect waves-light green left"><i class="material-icons">add</i></a>
                </div>
                
            </div>
            <div class="progress" style="visibility: hidden;">
                  <div class="indeterminate"></div>
            </div>
            <div class="card-panel error hide red accent-2" style="padding: 10px">
                <p class="white-text">Oops, something went wrong!</p>
            </div>
        </div>
        

        <script type="text/template" id="field-template">
            
                <div class="input-field col s6">
                    <input class="input-name validate" name="name" type="text" value="<%= name %>" />
                    <label for="name">Field Name</label>
                </div>
                <div class="col s5">
                    <label for="zd_map">Maps To Ticket Field</label>
                    <select name="zd_map" style="display:block" class="validate">
                            <option value="">--Select A Field To Map To--</option>
                        @foreach($formFields as $f)
                            <option value="{{$f->id}}">{{$f->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col s1">
                    <a class="waves-effect waves-light btn-floating right red field-delete hide"><i class="material-icons">delete</i></a>
                </div>
        </script>

        <script type="text/template" id="template-row">
            <td><input class="prop id-prop" type="text" value="<%= templateNumber %>" /></td>            
            <td>
                <a class="waves-effect waves-light btn-floating blue disabled template-save"><i class="material-icons">input</i></a>
                <a class="waves-effect waves-light btn-floating red template-delete "><i class="material-icons">delete</i></a>
            </td>
        </script>

        <script type="text/template" id="new-template-row">
            <td><input class="prop id-prop" data-field-id="template-identifier" type="text" value="" /></td>            
            @foreach($fields as $f)
                <td><input class="prop val-prop" type="text" data-field-id="{{ $f->id }}" value="" /></td>
            @endforeach           
            <td>
                <a class="waves-effect waves-light btn-floating blue disabled new-template-save"><i class="material-icons">input</i></a>
                <a class="waves-effect waves-light btn-floating red  template-delete"><i class="material-icons">delete</i></a>
                <i class="material-icons green-text hide save-done">check_circle</i>
            </td>
        </script>

        {{--<script type="text/javascript" src="https://assets.zendesk.com/apps/sdk/2.0/zaf_sdk.js"></script>--}}
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/underscore.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/backbone.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/vendor/jquery.stickytableheaders.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.js"></script>
        <script>
            var fields = {!! $fields->toJson() !!};
            var zdFields = {!! $formFields->toJson() !!};
        </script>
        <script type="text/javascript" src="{{ asset('js/fieldsApp.js') }}"></script>
    </body>
</html>
