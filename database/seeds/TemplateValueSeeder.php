<?php

use Illuminate\Database\Seeder;

class TemplateValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('template_values')->insert([
            'template_id' => 1,
            'template_field_id' => 1,
            'value' => 'Excel'
        ]);

        DB::table('template_values')->insert([
            'template_id' => 1,
            'template_field_id' => 2,
            'value' => 'Insurance Summary'
        ]);

        DB::table('template_values')->insert([
            'template_id' => 1,
            'template_field_id' => 3,
            'value' => ''
        ]);

        DB::table('template_values')->insert([
            'template_id' => 1,
            'template_field_id' => 4,
            'value' => 'N'
        ]);

        DB::table('template_values')->insert([
            'template_id' => 1,
            'template_field_id' => 4,
            'value' => 'N'
        ]);

        DB::table('template_values')->insert([
            'template_id' => 1,
            'template_field_id' => 4,
            'value' => 'Y'
        ]);
    }
}
