<?php

use Illuminate\Database\Seeder;

class TemplateFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('template_fields')->insert([
            'name' => 'Report System',
            'zendesk_field_id' => 48162208
        ]);

        DB::table('template_fields')->insert([
            'name' => 'Report Type',
            'zendesk_field_id' => 47188728
        ]);

        DB::table('template_fields')->insert([
            'name' => 'Away Held Asset',
            'zendesk_field_id' => 46478607
        ]);

        DB::table('template_fields')->insert([
            'name' => 'DPP/REIT',
            'zendesk_field_id' => 46478867
        ]);

        DB::table('template_fields')->insert([
            'name' => 'Performance Info',
            'zendesk_field_id' => 46478887
        ]);

        DB::table('template_fields')->insert([
            'name' => 'Manual Entry',
            'zendesk_field_id' => 46478647
        ]);
    }
}
