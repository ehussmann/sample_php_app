<?php

use Illuminate\Database\Seeder;
use App\Template;
use App\TemplateField;
use App\TemplateValue;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $file = fopen(__DIR__.'/templates_4.13.17.csv', 'r');
        $headers = fgetcsv($file, 1000, ",");
        $templates = [];
        while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
            // echo $i."\n";
            // print_r($data);
            $data = array_combine($headers, $data);
            array_push($templates, $data);
        }   

        $fields = TemplateField::all();

        foreach($templates as $template) {
            try {
                
            
                $templateModel = new Template;
                $templateModel->identifier = $template['Template Number'];
                $templateModel->save();

                foreach($fields as $field) {
                    $valueData = [
                        "template_id" => $templateModel->id,
                        "template_field_id" => $field->id,
                    ];

                    switch ($field->name) {
                        case 'Report System':
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, 'System');
                            break;
                        case 'Report Type':
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, 'Report Type');
                            break;
                        case 'Away Held Asset':
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, 'Away Held');
                            break;
                        case 'DPP/REIT':
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, 'DPP/REIT');
                            break;
                        case 'Performance Info':
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, 'Performance Info');
                            break;
                        case 'Manual Entry':
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, 'Manual Entry');
                            break;
                        case 'Benefit Reporting':
                        case 'Ins Summary':
                        case 'Ins Illustration':
                        case 'Ins Allocation':
                        case 'Ins Comparison':
                        case 'Status':
                            $name = $field->name;
                            $valueData['value'] = $this->getFieldOptionValue($field, $template, $name);
                            break;
                        case 'Status Date':
                            $valueData['value'] = $template['Status Date'];
                            break;

                        
                    }

                    // print_r($valueData);
                    $templateModel->values()->create($valueData);
                    // $template->load('values');
                }

                if(!is_null($templateModel)) {
                    print $templateModel->identifier." created \n";
                } else {
                    print "Could NOT CREATE template ".$template['Template Number']."\n";
                }

            } catch (Exception $e) {
                print $e->getTraceAsString()."\n";
            }        
        }


        
    }

    private function getFieldOptionValue($field, $template, $templateKey) {
        $templateValue = $template[$templateKey];
        if($templateValue == 'Y') {
            $templateValue = 'Yes';
        } 
        if($templateValue == 'N') {
            $templateValue = 'No';
        }
        // print $templateValue;   
        $options = collect(json_decode($field->tagger_options));
        // print_r($options);
        $opt = $options->where('name', $templateValue)->first();
        // print_r($opt);
        if(!is_null($opt)) {
            return $opt->value;
        } else {
            return '';
        }

    }
}
