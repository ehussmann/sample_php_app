<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Dashboard')->middleware('zd');
Route::get('dashboard', 'Dashboard')->middleware('zd');
Route::get('field', 'TemplateFieldController@index')->middleware('zd');
Route::get('sidebar', 'Sidebar')->middleware('zd');

Route::resource('template','TemplateController', ['only'=>['store', 'update', 'destroy']]);
Route::resource('field','TemplateFieldController', ['only'=>['store', 'update', 'destroy']]);
Route::resource('value','TemplateValueController', ['only'=>['store', 'update']]);